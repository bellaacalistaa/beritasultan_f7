
var routes = [
  // Index page
  {
    path: '/',
    url: './index.html',
    name: 'page-home',
    on: {
      pageInit(e, page) {
        getNewBerita(),
          getBeritaTeratas(),
          getAllBerita()
      }
    }
  },
  // Home page 2
  {
    path: '/home/',
    url: './pages/home-second.html',
    name: 'page-home-second',
    on: {
      pageInit(e, page) {
        getBeritaUnggulan(),
        getAllBerita(),
        getBeritaLainnya()
      }
    }
  },
  // Home page 3
  {
    path: '/homethird/',
    url: './pages/home-third.html',
    name: 'page-home-third',
    on: {
      pageInit(e, page) {
        getAllTag(),
        getBeritaCrypto()
      }
    }
  },
  // Profile
  {
    path: '/profile/',
    url: './pages/profile.html',
    name: 'page-profile',
  },
  // Single
  {
    path: '/single/:id/',
    url: './pages/single.html',
    name: 'page-single',
    on: {
      pageInit: function (e, page) {
        getDetailBerita(page.router.currentRoute.params.id),
        getBeritaUnggulan()
      },
      pageReInit: function (e, page) {
        getDetailBerita(page.router.currentRoute.params.id),
        getBeritaUnggulan()
      },
    }
  },
  // Blog Style 1
  {
    path: '/blogone/',
    url: './pages/blog-one.html',
    name: 'page-blog-one',
  },
  // Blog Style 2
  {
    path: '/blogtwo/',
    url: './pages/blog-two.html',
    name: 'page-blog-two',
  },
  // Blog Style 3
  {
    path: '/blogthree/',
    url: './pages/blog-three.html',
    name: 'page-blog-three',
  },
  // Blog Style 4
  {
    path: '/blogfour/',
    url: './pages/blog-four.html',
    name: 'page-blog-four',
  },
  // Blog Style 5
  {
    path: '/blogfive/',
    url: './pages/blog-five.html',
    name: 'page-blog-five',
  },
  // Blog Style 6
  {
    path: '/blogsix/',
    url: './pages/blog-six.html',
    name: 'page-blog-six',
  },
  // Pages
  {
    path: '/listpages/',
    url: './pages/list-pages.html',
    name: 'page-pages',
  },
  // elemet 1
  {
    path: '/element1/',
    url: './pages/element1.html',
    name: 'element1',
  },
  // elemet 2
  {
    path: '/element2/',
    url: './pages/element2.html',
    name: 'element2',
  },
  // elemet 3
  {
    path: '/element3/',
    url: './pages/element3.html',
    name: 'element3',
  },
  // elemet 4
  {
    path: '/element4/',
    url: './pages/element4.html',
    name: 'element4',
  },
  // elemet 5
  {
    path: '/element5/',
    url: './pages/element5.html',
    name: 'element5',
  },
  // elemet 6
  {
    path: '/element6/',
    url: './pages/element6.html',
    name: 'element6',
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];