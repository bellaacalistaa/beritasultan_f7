"use strict";
// Dom7
var $ = Dom7;

// Init App
var app = new Framework7({
  root: '#app',
  theme: 'md',
  routes: routes,
  view: {
    pushState: false,
  },
  stackPages: true,
  on: {
    pageInit: function (page) {
      feather.replace();

      /*
        var masonry = new Macy({
          container: '#masonry',
          mobileFirst: true,
          columns: 1,
          margin: {
            y: 10,
            x: 10,
          },
          breakAt: {
            320: 2,
            360: 2,
            576: 3,
            600: 3,
            768: 3,
            1024: 4,
          },
        });
      */

      let swiper = new Swiper('.list-posts-full-bg-swipper', {
        loop: true,
        spaceBetween: 20,
        breakpoints: {
          320: {
            "slidesPerView": "1.1"
          },
          400: {
            "slidesPerView": "1.4"
          },
          768: {
            "slidesPerView": "2.5"
          },
          1024: {
            "slidesPerView": "3.5"
          },
          600: {
            "slidesPerView": "2.2"
          },
          240: {
            "slidesPerView": "1.1"
          },
          834: {
            "slidesPerView": "3.2"
          },
          1920: {
            "slidesPerView": "6.3"
          },
          360: {
            "slidesPerView": "1.2"
          },
          576: {
            "slidesPerView": "2.2"
          }
        },
        on: {
          transitionEnd: function () {
            let rgba = $(".swiper-slide-active").attr("data-color");
            $(".newsman-page-gradient").css("background-color", rgba);
            console.log(rgba);
          },
        },
      });
      // Authors list
      let swiperAuthors = new Swiper('.authors-card .swiper-container', {
        loop: true,
        spaceBetween: 10,
        breakpoints: {
          320: {
            "slidesPerView": "2.1"
          },
          400: {
            "slidesPerView": "2.5"
          },
          768: {
            "slidesPerView": "4.5"
          },
          1024: {
            "slidesPerView": "5.5"
          },
          600: {
            "slidesPerView": "3.2"
          },
          240: {
            "slidesPerView": "1.1"
          },
          834: {
            "slidesPerView": "4.5"
          },
          1920: {
            "slidesPerView": "6.3"
          },
          360: {
            "slidesPerView": "2.1"
          },
          576: {
            "slidesPerView": "3.2"
          }
        },
      });
      // Card posts
      let swiperCard = new Swiper('.list-posts-card .swiper-container', {
        loop: true,
        spaceBetween: 20,
        breakpoints: {
          320: {
            "slidesPerView": "1.1"
          },
          400: {
            "slidesPerView": "1.4"
          },
          768: {
            "slidesPerView": "2.5"
          },
          1024: {
            "slidesPerView": "3.5"
          },
          600: {
            "slidesPerView": "2.2"
          },
          240: {
            "slidesPerView": "1.1"
          },
          834: {
            "slidesPerView": "3.2"
          },
          1920: {
            "slidesPerView": "6.3"
          },
          360: {
            "slidesPerView": "1.2"
          },
          576: {
            "slidesPerView": "2.2"
          }
        }
      });
    },
  }

});

function getNewBerita() {
  $("#beritanew").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getNew',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      var str = "";
      response.data.forEach(data => {
        str = ` <div class="swiper-slide" data-color="rgb(214, 63, 168)">
                      <div class="post-full-bg">
                        <div class="post-content margin-top display-flex flex-direction-column justify-content-flex-end newsman-object-fit">
                          <img src="./images/blog_foto/${data.foto_preview}" alt="1">
                          <div class="post-title">
                            <a href="/single/${data.id}/">
                              <h2 class="no-margin-bottom">${data.nama}</h2>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>`
        $('#swiper-content').append(str);
      })
    }
  })
}

function getBeritaTeratas() {
  $("#beritateratas").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getMonthly',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      var str = "";
      response.data.forEach(data => {
        str = `<div class="blog-list display-flex align-items-start flex-direction-row-reverse">
        <div class="blog-list-img newsman-object-fit">
          <img src="./images/blog_foto/${data.foto_preview}" alt="bl1">
        </div>
        <div class="blog-list-infos margin-right margin-top">
          <h2 class="margin-bottom-half no-margin-top"><a href="/single/${data.id}/">${data.nama}</a></h2>
          <span>${data.tanggal}</span>
        </div>
      </div>`
        $('#beritateratas').append(str);
      })
    }
  })
}

function getAllBerita() {
  $("#masonry").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getAllBerita',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      var str1 = "";
      var str2 = "";
      response.data.forEach(data => {
        str1 = `<div class="masonry-item">
        <img src="./images/blog_foto/${data.foto_preview}" alt="m1" class="masonry-img">
        <div class="post-title">
          <a href="/single/${data.id}/">
            <h2 class="no-margin-bottom">${data.nama}</h2>
          </a>
        </div>
      </div><br>`
        $('#masonry').append(str1);
        str2 = `<div class="blog-list display-flex align-items-start">
        <div class="blog-list-img newsman-object-fit">
          <img src="./images/blog_foto/${data.foto_preview}" alt="bl1">
        </div>
        <div class="blog-list-infos margin-left">
          <div class="newsman-badge">
            <a href="#" class="badge color-purple text-color-white">${data.blogType}</a>
          </div>
          <h2 class="margin-bottom-half"><a href="/single/${data.id}/">${data.nama}</a></h2>
          <span>${data.tanggal}</span>
        </div>
      </div>`;
      $('#berita_lain').append(str2);
      })      
    }
  })
}

function getDetailBerita(id) {
  console.log(id);
  $("#single-content").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getDetailBerita',
    type: 'get',
    data: {
      id: id
    },
    dataType: 'json',
    success: function (response) {
      var str = "";
      response.data.forEach(data => {
        str = `<img src="./images/blog_foto/${data.foto}" alt="bp1"><div class="newsman-badge">
        <a href="#" class="badge color-red text-color-white">${data.blogType}</a>
      </div><h2>${data.nama}</h2>${data.keterangan}`;
      })
      $('#single-content').append(str);
    }
  })
}

function getBeritaUnggulan() {
  console.log('unggul');
  $("#berita_unggulan").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getBeritaUnggulan',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      console.log(response.data);
      var str1 = "";
      var str2 = "";
      response.data.forEach(data => {
        str1 = `<div class="blog-list display-flex align-items-start">
        <div class="blog-list-img newsman-object-fit">
          <img src="./images/blog_foto/${data.foto_preview}" alt="bl1">
        </div>
        <div class="blog-list-infos margin-left">
          <div class="newsman-badge">
            <a href="#" class="badge color-purple text-color-white">${data.blogType}</a>
          </div>
          <h2 class="margin-bottom-half"><a href="/single/${data.id}/">${data.nama}</a></h2>
          <span>${data.tanggal}</span>
        </div>
      </div>`;
      $('#berita_unggulan').append(str1);
      str2 = `<div class="swiper-slide">
        <div class="post-card">
          <div class="post-card-img display-flex flex-direction-column justify-content-flex-end newsman-object-fit">
            <img src="./images/blog_foto/${data.foto_preview}" alt="c2">
            <h3><a href="/single/${data.id}/">${data.nama}</a></h3>
          </div>
        </div>
      </div>`;
      $('#slide_unggulan').append(str2);
      })
      
    }
  })
}


function getBeritaLainnya() {
  $("#big_content").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getBeritaLainnya',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      console.log(response.data);
      var str = "";
      response.data.forEach(data => {
        str = `<img src="./images/blog_foto/${data.foto_preview}" alt="bp1">
        <div class="margin-top">
          <div class="newsman-badge">
            <a href="#" class="badge color-red text-color-white">${data.blogType}</a>
          </div>
          <h3><a href="/single/${data.id}/">${data.nama}</a></h3>`;
      $('#big_content').append(str);
      })
      
    }
  })
}

function getAllTag() {
  $("#tag_content").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getAllTag',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      console.log(response.data);
      var str = "";
      response.data.forEach(data => {
        str = `<div class="newsman-badge">
        <a href="#" class="badge bg-color-white text-color-black">${data.nama}</a>
      </div>`;
      $('#tag_content').append(str);
      })
      
    }
  })
}

function getBeritaCrypto() {
  $("#berita_grid").empty();
  Framework7.request({
    url: 'https://api.beritasultan.com/getBeritaCrypto',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      console.log(response.data);
      var str = "";
      response.data.forEach(data => {
        str = `<div class="post-grid">
        <div class="post-grid-img newsman-object-fit">
          <img src="./images/blog_foto/${data.foto_preview}" alt="g1">
        </div>
        <div class="blog-list-infos">
          <span class="display-block padding-top-half">${data.tanggal}</span>
          <h2 class="margin-top-half">
            <a href="/single/${data.id}/">${data.nama}</a>
          </h2>
        </div>
      </div>`;
      $('#berita_grid').append(str);
      })
      
    }
  })
}

function search() {
  Framework7.request({
    url: 'https://api.beritasultan.com/search',
    type: 'get',
    dataType: 'json',
    success: function (response) {
      console.log(response.data);
      var str = "";
      response.data.forEach(data => {
        str = `<div class="post-grid">
        <div class="post-grid-img newsman-object-fit">
          <img src="./images/blog_foto/${data.foto_preview}" alt="g1">
        </div>
        <div class="blog-list-infos">
          <span class="display-block padding-top-half">${data.tanggal}</span>
          <h2 class="margin-top-half">
            <a href="/single/${data.id}/">${data.nama}</a>
          </h2>
        </div>
      </div>`;
      $('#berita_grid').append(str);
      })
      
    }
  })
}

// Menu
$("body").on("click", ".toolbar .link", function (e) {
  $(".link.tab-link-active").removeClass("tab-link-active");
  $(this).addClass("tab-link-active");
});


$(document).on('page:init', '.page[data-name="page-blog-four"]', function (e) {
  let masonry2 = new Macy({
    container: '#masonry-two',
    mobileFirst: true,
    columns: 1,
    margin: {
      y: 10,
      x: 10,
    },
    breakAt: {
      320: 2,
      360: 2,
      576: 3,
      600: 3,
      768: 3,
      1024: 4,
    },
  });
})